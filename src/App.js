import Form from "./Materi/Form";
import Hooks from "./Materi/Hooks";
import Komponen from "./Materi/Komponen";
import Lifecycle from "./Materi/Lifecycle";
import Redux from "./Materi/Redux";
import Rendering from "./Materi/Rendering";
import Styling from "./Styling";

function App() {
  return (
    <div className="App">
      {/* <Komponen /> */}
      {/* <Styling /> */}
      {/* <Rendering /> */}
      {/* <Form /> */}
      {/* <Lifecycle /> */}
      {/* <Hooks /> */}
      <Redux />
    </div>
  );
}

export default App;
