import React from "react";
import Reusable from "../Materi/Reusable";
import Bootstrap from "./Bootstrap";
import Inline from "./Inline";
import Module from "./Module";
import Plain from "./Plain";
import Sass from "./Sass";
import Styled from "./Styled";

export default class Styling extends React.Component {
  render() {
    return (
      <div>
        {/* <Plain /> */}
        {/* <Sass /> */}
        {/* <Inline /> */}
        {/* <Module /> */}
        {/* <Styled /> */}
        {/* <Bootstrap /> */}
        <Reusable />
      </div>
    );
  }
}
