import Routing from "./Materi/Routing";

function AppRouting() {
  return (
    <div className="App">
      <header className="App-header">
        <Routing />
      </header>
    </div>
  );
}

export default AppRouting;
