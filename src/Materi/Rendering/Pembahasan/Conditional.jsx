import React from "react";

export default class Conditional extends React.Component {
  state = {
    isLoading: false,
  };

  render() {
    // const isLogin = false;
    setTimeout(() => {
      this.setSstate({
        isLoading: false,
      });
    }, 3000);
    // let message = "";

    // if (isLogin) {
    //   return (
    //     <div>
    //       <h1>anda sudah login</h1>
    //     </div>
    //   );
    // } else {
    //   message = "anda belum login";
    // } else {
    //   message = "silahkan login terlebih dahulu";
    // }
    return (
      <div>
        {this.state.isLoading ? (
          <h1>Loading...</h1>
        ) : (
          <h1>Selamat datang di kelas MERN</h1>
        )}
      </div>
    );
  }
}
