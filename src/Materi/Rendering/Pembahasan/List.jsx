import React from "react";

export default class List extends React.Component {
  state = {
    user: ["Edi", "Aldo", "Wawan", "Mahesa", "Edo"],
  };
  render() {
    return (
      <div>
        <ul>
          {this.state.user.map((user, i) => (
            <li key={i}>{user}</li>
          ))}
        </ul>
      </div>
    );
  }
}
