import { useEffect, useState } from "react";

const Effect = () => {
  let [text, setText] = useState("");
  let [user, setUser] = useState([]);
  let [second, setSecond] = useState(0);

  //pemangiilan API
  useEffect(() => {
    console.log("get API");
    fetch("https://jsonplaceholder.typicode.com/users/1")
      .then((response) => response.json())
      .then((response) => setUser(response));
  }, []);

  useEffect(() => {
    localStorage.setItem("text", text);
    //tidak ada dependency (tanpa array text, hanya console log)
    console.log("singkronisasi");
    //dependency state
  }, [text]);

  useEffect(() => {
    console.log("interval");
    let intervalid = setInterval(() => {
      setSecond((s) => s + 1);
    }, 1000);

    return () => {
      console.log("clear interval");
      clearInterval(intervalid);
    };
  }, []);

  return (
    <div>
      <textarea cols="30" rows="10" onChange={(e) => setText(e.target.value)} />
      <p>{user.name}</p>
      <div>
        Waktu: {Math.floor(second / 60)} Menit {second % 60} Detik
      </div>
    </div>
  );
};

export default Effect;
