import { useState } from "react";
import Counter from "./Pembahasan/Counter";
import Effect from "./Pembahasan/Effect";
import { useToogle } from "./Pembahasan/Hook/useToogle";
import Identity from "./Pembahasan/Identity";

const Hooks = () => {
  let [label, setLabel] = useState("ON");
  let [lampu, setLampu] = useToogle();

  const style = {
    backgroundColor: lampu ? "red" : "green",
    textAlign: "center",
    height: "400px",
  };

  const handleSaklar = () => {
    setLampu(!lampu);
    setLabel((e) => {
      if (e === "ON") {
        return "OFF";
      }
      return "ON";
    });
  };

  return (
    <div style={style}>
      {/* <Counter /> */}
      {/* <Effect /> */}
      {/* <Identity /> */}
      <button onClick={handleSaklar}> {label}</button>
    </div>
  );
};

export default Hooks;
