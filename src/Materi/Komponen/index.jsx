import React from "react";
import ClassComponent from "./Pembahasan/ClassComponent";
import FunctionalComponent from "./Pembahasan/FunctionalComponent";
import TaskComponent from "./Pembahasan/TaskComponent";

class Komponen extends React.Component {
  render() {
    return (
      <div>
        <ClassComponent nama="Dodi Prakoso" />
        <FunctionalComponent nama={23} />
        <TaskComponent desc="Task Eduwork" />
      </div>
    );
  }
}

export default Komponen;
