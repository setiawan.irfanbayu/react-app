import React from "react";

class TaskComponent extends React.Component {
  state = {
    name: "Irfan Bayu",
  };

  render() {
    return (
      <div>
        {/* props */}
        <h1>Komponen ini adalah {this.props.desc}</h1>
        {/* state */}
        <h2>Hello {this.state.name}</h2>
      </div>
    );
  }
}

export default TaskComponent;
