import React, { useState } from "react";

const FunctionalComponent = ({ nama }) => {
  const [value, setvalue] = useState(0);

  const handlePlus = () => {
    setvalue(value + 1);
  };

  const handleMinus = () => {
    if (value > 0) {
      setvalue(value - 1);
    }
  };

  return (
    <div>
      <h1>Membuat komponen dengan Functional Component</h1>
      <h2>Hallo {nama} selamat belajar</h2>
      <button onClick={handleMinus}>-</button>
      <span>{value}</span>
      <button onClick={handlePlus}>+</button>
    </div>
  );
};

FunctionalComponent.defaultProps = {
  nama: "User",
};

export default FunctionalComponent;
