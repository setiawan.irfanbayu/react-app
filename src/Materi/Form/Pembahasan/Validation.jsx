import React from "react";
import * as Validator from "validatorjs";

const Input = ({ label, type, name, onChange }) => {
  return (
    <div>
      <label>{label} :</label>
      <br />
      <input
        type={type}
        name={name}
        onChange={(e) => onChange(e.target.value)}
      />
      <br />
    </div>
  );
};

const ShowErrors = ({ error }) => {
  return (
    <ul style={{ color: "red", marginLeft: "-20px" }}>
      {error.map((error, i) => (
        <li key={i}>{error}</li>
      ))}
    </ul>
  );
};

class Validation extends React.Component {
  state = {
    email: "",
    password: "",
    error: [],
  };

  handleSubmit = (event) => {
    event.preventDefault();
    const { email, password } = this.state;

    let data = {
      email,
      password,
    };

    let rules = {
      email: "required|email",
      password: "min:8|required",
    };

    let validation = new Validator(data, rules);

    validation.passes();
    this.setState({
      error: [
        ...validation.errors.get("email"),
        ...validation.errors.get("password"),
      ],
    });

    // let message = [];

    // if (email.length === 0) {
    //   message = [...message, "Email harus diisi"];
    // }

    // if (password.length === 0) {
    //   message = [...message, "Password harus diisi"];
    // }

    // const re =
    //   /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    // if (!re.test(String(email).toLowerCase())) {
    //   message = [...message, "Email tidak valid"];
    // }

    // if (password.length < 8) {
    //   message = [...message, "Password minimal 8 karakter"];
    // }

    // if (message.length > 0) {
    //   this.setState({ error: message });
    // } else {
    //   alert(`
    //     nama : ${this.state.email}
    //     jurusan : ${this.state.password}
    //   `);
    //   this.setState({ error: [] });
    // }
  };

  render() {
    const style = {
      width: "400px",
      margin: "100px auto",
      border: "1px solid black",
      padding: "20px",
    };
    return (
      <div style={style}>
        <h4>Login Page</h4>
        {/* <ShowErrors error={this.state.error} /> */}
        {this.state.error && <ShowErrors error={this.state.error} />}
        <form onSubmit={this.handleSubmit}>
          <Input
            label="Email"
            type="email"
            name="email"
            onChange={(value) => this.setState({ email: value })}
          />
          <Input
            label="Password"
            type="password"
            name="password"
            onChange={(value) => this.setState({ password: value })}
          />
          <br />
          <button type="submit">Login</button>
        </form>
      </div>
    );
  }
}

export default Validation;
