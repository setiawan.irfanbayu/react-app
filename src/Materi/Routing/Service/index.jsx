import { Routes, Route, useMatch } from "react-router-dom";
import { Link } from "react-router-dom";

const Service = () => {
  const match = useMatch();

  return (
    <div className="main">
      <h2>Service Page</h2>
      <ul>
        <li>
          <Link to={`${match.url}/computer`}>Computer</Link>
        </li>
        <li>
          <Link to={`${match.url}/smarthphone`}>Smartphone</Link>
        </li>
      </ul>
      <hr />
      <Routes>
        <Route path={`${match.url}/computer`}>
          Komputer, CPU, Monitor, dll
        </Route>
        <Route path={`${match.url}/smarthphone`}>
          ASUS, Iphone, Xiaomi, dll
        </Route>
      </Routes>
    </div>
  );
};

export default Service;
