import Navigation from "./Navigation";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Home from "./Home";
import Service from "./Service";
import Client from "./Client";
import Contact from "./Contact";
import Post from "./Post";

const Routing = () => {
  return (
    <div>
      <Router>
        <Navigation />
        <Routes>
          <Route exact path="/" element={<Home />} />
          <Route path="/service" element={<Service />} />
          <Route path="/client" element={<Client />} />
          <Route path="/contact" element={<Contact />} />
          <Route path="/posts/:id" element={<Post />} />
        </Routes>
      </Router>
    </div>
  );
};

export default Routing;
