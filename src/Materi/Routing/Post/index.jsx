import { useParams } from "react-router-dom";
import { useHistory } from "react-router-dom";

const Post = () => {
  // const history = useHistory();
  const { id } = useParams();
  return (
    <div className="main">
      <h2>Post ke-{id}</h2>
      <p>Deskripsi</p>
      {/* <button onClick={() => history.push("/")}>kembali</button> */}
    </div>
  );
};

export default Post;
