import { Link } from "react-router-dom";

const Home = () => {
  return (
    <div className="main">
      <h2>Home Page</h2>
      <ul>
        <li>
          <Link to="/post/1">satu</Link>
        </li>
        <li>
          {" "}
          <Link to="/post/2">dua</Link>
        </li>
        <li>
          {" "}
          <Link to="/post/3">tiga</Link>
        </li>
      </ul>
      <p>
        Lorem, ipsum dolor sit amet consectetur adipisicing elit. Tempora
        adipisci alias itaque esse. Adipisci deleniti ad beatae consectetur.
        Harum, consequuntur. Consequuntur dolores natus id aut quod quibusdam,
        numquam nihil neque.
      </p>
    </div>
  );
};

export default Home;
